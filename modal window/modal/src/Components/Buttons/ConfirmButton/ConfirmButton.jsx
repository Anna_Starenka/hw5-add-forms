import './ConfirmButton.scss'

export default function ConfirmButton(props)  {

    const { text, onClick, className } = props
    return (

        <>
            <button className={className} onClick={onClick}>{text}</button>

        </>
    )

};
