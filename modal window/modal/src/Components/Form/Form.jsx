import { useDispatch } from "react-redux";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import "./form.scss";
import { formClose, modalSubmitOpen } from "../../reducers/modal.reducer";

export default function CheckoutForm() {
  const dispatch = useDispatch();

  const formInfo = (values) => {
    const userCartInfo = JSON.parse(localStorage.getItem("cart"));
    console.log(values, userCartInfo);
    dispatch(formClose());
    dispatch(modalSubmitOpen());
  };

  const inititalValues = {
    firstName: "",
    lastName: "",
    age: "",
    adress: "",
    phone: "",
  };

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required("First name is required"),
    lastName: Yup.string().required("Last name is required"),
    age: Yup.number().required("Age is required"),
    adress: Yup.string()
      .required("Adress is required")
      .min(5, "Address is too short"),
    phone: Yup.number().required("Phone number is required"),
  });

  return (
    <Formik
      initialValues={inititalValues}
      onSubmit={(values) => formInfo(values)}
      validationSchema={validationSchema}
    >
      <Form>
        <header className="form-header">
          <h2 className="form-title">Please,fill the form</h2>
        </header>

        <div>
          <Field
            type="text"
            id="firstName"
            name="firstName"
            placeholder="First name"
          />
          <ErrorMessage name="firstName" className="error-message" component="div" />
        </div>
        <div>
          <Field
            type="text"
            id="lasttName"
            name="lastName"
            placeholder="Last name"
          />
          <ErrorMessage name="lastName" className="error-message" component="div" />
        </div>
        <div>
          <Field
            type="number"
            id="age"
            name="age"
            min="18"
            max="100"
            placeholder="Age"
          />
          <ErrorMessage name="age" className="error-message" component="div" />
        </div>
        <div>
          <Field type="text" id="adress" name="adress" placeholder="Adress" />
          <ErrorMessage name="adress" className="error-message" component="div" />
        </div>
        <div>
          <Field
            type="tel"
            id="phone"
            name="phone"
            placeholder="Phone 000-000-0000"
          />
          <ErrorMessage name="phone" className="error-message" component="div" />
        </div>
        <button type="submit" className="submit-btn">
          Checkout
        </button>
      </Form>
    </Formik>
  );
}
