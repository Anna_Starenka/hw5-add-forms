import { NavLink } from "react-router-dom";
import CartLogo from "../Icons/CartLogo";
import FavouriteLogo from "../Icons/FavouriteLogo";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import "./Header.scss";

export default function Header() {
  const cartCount = useSelector((state) => state.cart.cartToLocal);
  const favCount = useSelector((state) => state.favourite.favouriteToLocal);

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cartCount));
    localStorage.setItem("favourite", JSON.stringify(favCount));
  }, [cartCount, favCount]);
  return (
    <>
      <div className="header">
        <NavLink to="/" style={{ textDecoration: "none" }}>
          <h1 className="title">clothes for you</h1>
        </NavLink>

        <div className="header-svg-wrapp">
          <sup className="counter">{favCount.length}</sup>
          <NavLink to="/favourites">
            <FavouriteLogo />
          </NavLink>
          <sup className="counter">{cartCount.length}</sup>
          <NavLink to="/cartitems">
            <CartLogo />
          </NavLink>
        </div>
      </div>
    </>
  );
}
