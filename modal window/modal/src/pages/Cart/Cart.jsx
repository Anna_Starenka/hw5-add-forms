import Modal from "../../Components/Modal/Modal";
import { useDispatch, useSelector } from "react-redux";
import {
  removeFromCart,
  modalClose,
  modalOpen,
  modalSubmitClose,
  emptyCart,
} from "../../reducers";
import "./cart.scss";
import CheckoutForm from "../../Components/Form/Form";

export default function Cart() {
  const dispatch = useDispatch();

  const modal = useSelector((state) => state.modal.isModal);
  const cart = useSelector((state) => state.cart.cartToLocal);
  const submission = useSelector((state) => state.modal.isModalSubmit);

  return (
    <>
      {!cart.length && (
        <div className="cart-empty">
          <strong> Cart is empty</strong>
        </div>
      )}
      <h1 className="title-main">Cart</h1>
      <div className="products">
        {cart.map((el) => (
          <div key={el.id}>
            <div className="wrapper">
              <div className="image">
                <img className="image" src={el.image} alt={el.name} />
              </div>
              <div className="card-main">
                <div className="card-body">
                  <h1>{el.name}</h1>
                  <p className="art">Art: {el.article}</p>
                  <p className="color">Color: {el.color}</p>
                </div>
                <div className="card-footer">
                  <p className="price">UAH {el.price}</p>
                  <span
                    className="remove-card"
                    onClick={() => {
                      dispatch(modalOpen());
                    }}
                  >
                    X
                  </span>
                </div>
              </div>

              {modal && (
                <Modal
                  text="Do you want to delete this product from cart?"
                  onCancel={() => {
                    dispatch(modalClose());
                  }}
                  onConfirm={() => {
                    dispatch(removeFromCart(el));
                    dispatch(modalClose());
                  }}
                />
              )}
            </div>
          </div>
        ))}
      </div>
      {cart.length >= 1 && <CheckoutForm />}
      {submission && (
        <Modal
          text="Thank you for choosen our product!"
          onCancel={() => {
            dispatch(modalSubmitClose());
          }}
          onConfirm={() => {
            dispatch(emptyCart());
            dispatch(modalSubmitClose());
          }}
        />
      )}
    </>
  );
}
