import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { modalClose, addToCart } from "../../reducers";
import Modal from "../../Components/Modal/Modal";
import ProductCard from "../../Components/ProductCard/ProductCard";

import "./Home.scss";

export default function Home() {
  const dispatch = useDispatch();

  const fetchData = useSelector((state) => state.data.data);
  const modal = useSelector((state) => state.modal.isModal);

  const [selectedProduct, setSelectedProduct] = useState([]);

  return (
    <>
      <div>
        <h1 className="title-main">Clothes</h1>
        <div className="products-wrapper">
          {!fetchData
            ? "Loading"
            : fetchData.map((product) => (
                <ProductCard
                  key={product.id}
                  setProduct={() => setSelectedProduct(product)}
                  item={product}
                />
              ))}
        </div>
      </div>

      {modal && (
        <Modal
          text="Do you want to add this product to the cart?"
          onCancel={() => {
            dispatch(modalClose());
          }}
          onConfirm={() => {
            dispatch(addToCart(selectedProduct));
            dispatch(modalClose());
          }}
        />
      )}
    </>
  );
}
